Introduction:
-------------------
Fetch all public FB page feeds for individual domain in a multi domain site.
This creates block for each domain which can be used on site to display FB 
page feeds. 
All we need to login using individual domain url and add facebook page url.


Installation:
------------
 
 * This module can be installed as simple as any contributed module need 
installation. 
1  Download the zip file and extract it. 
2. Place the module folder in sites/all/modules/
3. Enable this module from admin back end at admin/modules

How Configure:
-----------------
1. Go to Configuration - Content Authoring - Facebook feed configuration.
2. Enter Public Facebook page URL and save.
